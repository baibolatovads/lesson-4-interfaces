package main
import(
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"reflect"
	"strconv"
)

//interface with Area() function
type myShape interface {
	Area() float64
}

type Shape struct{
	Type string `json:"type"`
	Radius interface{} `json:"radius, omitempty"`
	Height interface{} `json:"height, omitempty"`
	SideA interface{} `json:"a, omitempty"`
	SideB interface{} `json:"b, omitempty"`
	SideC interface{} `json:"c, omitempty"`
}

type Circle struct{
	Radius float64
}

//S = πR^2
func (c * Circle) Area() float64{
	return math.Pi * c.Radius * c.Radius
}

type Rectangle struct{
	A float64
	B float64
}

//S = ab
func (r * Rectangle) Area() float64{
	return r.A * r.B
}

type Triangle struct{
	Side float64
	Height float64
}

//S = 1/2*A*H
func (t * Triangle) Area() float64{
	return 0.5 * t.Side * t.Height
}

func ConvertToFloat64 (i interface{}) (float64, error){
	reflectType := reflect.TypeOf(i)
	reflectValue := reflect.ValueOf(i)
	switch reflectType.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return float64(reflectValue.Int()), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return float64(reflectValue.Uint()), nil
	case reflect.Float32, reflect.Float64:
		return reflectValue.Float(), nil
	case reflect.String:
		val, err := strconv.ParseFloat(reflectValue.String(), 64)
		if err != nil {
			panic(err.Error())
		}
		return val, nil
	default:
		panic(errors.New("Incorrect data provided!"))
	}
}

func (shape Shape) Area() float64 {
	switch shape.Type {
	case "circle":
		Radius, err := ConvertToFloat64(shape.Radius)
		if err != nil {
			return 0
		}
		circle := Circle{Radius: Radius}
		return circle.Area()
	case "rectangle":
		sideA, err := ConvertToFloat64(shape.SideA)
		if err != nil {
			return 0
		}
		sideB, err := ConvertToFloat64(shape.SideB)
		if err != nil {
			return 0
		}
		rec := Rectangle{A: sideA, B: sideB}
		return rec.Area()
	case "triangle":
		sideA, err := ConvertToFloat64(shape.SideA)
		if err != nil {
			return 0
		}
		Height, err := ConvertToFloat64(shape.Height)
		if err != nil {
			return 0
		}
		triangle := Triangle{Side: sideA, Height: Height}
		return triangle.Area()

		default:
			return 0
	}
}

func main() {
	myFile, _ := ioutil.ReadFile("my_shapes.json")
	fmt.Println("OK")
	shapes := []Shape{}
	_ = json.Unmarshal(myFile, &shapes)
	for _, shape := range shapes {
		area := shape.Area()
		shapeJson, _ := json.Marshal(shape)
		fmt.Println(string(shapeJson), area)
		}

	}

